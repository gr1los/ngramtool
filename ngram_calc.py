# Author: Gabriel Chaviaras <chaviaras10@gmail.com>

"""
This file includes the NGram_Calc classs.
"""

from difflib import SequenceMatcher
from nltk.util import ngrams
from nltk import FreqDist

class NGram_Calc(object):
    """
    This class is responsible for retrieving data from the provided file.
    Also tokenizes the file and then finds and generates the frequency
    distribution printing the 10 most common ngrams of the file.
    """

    def __init__(self, input_file):
        self.input_file = input_file

    def find_grams(self):
        """
        This function generates the bigrams, trigrams and fourgrams.
        Also calls the functions for distribution frequency generation.
        """

        tokens = open(self.input_file, 'r').read().split()

        if not tokens:
            print 'File is empty. Exiting'
            return

        bigrams = list(ngrams(tokens, 2))
        trigrams = list(ngrams(tokens, 3))
        fourgrams = list(ngrams(tokens, 4))

        self.count_freq(bigrams)
        self.count_freq(trigrams)
        self.count_freq(fourgrams)

    def count_freq(self, grams):
        """
        This function calculates the frequency distribution of the
        ngrams. After that removes and duplications using as
        comparator the similarity function.

        :param grams: A list including grams from tokens
        :type grams: list
        """
        # compute frequency distribution for all the ngrams
        fdist = FreqDist(grams)

        # remove the duplicates
        for group1 in fdist.keys():
            for group2 in fdist.keys():
                if group1 != group2:        # avoid same iterators
                    if self.similar(''.join(group1), ''.join(group2)):
                        # update the first group with the frequency of the
                        # second one, after that remove the duplicated
                        # group from the frequency dictionary
                        fdist[group1] += fdist[group2]
                        del fdist[group2]   # remove the duplicate

        print '10 most common ' + str(len(grams[0])) + '-grams:'
        for elem in fdist.most_common(10):
            gram = elem[0]
            freq = elem[1]
            print '"' + ', '.join(gram) + '"' + ' found ' + str(freq) + ' times.'
        print

    def similar(self, a, b):
        """
        A simple function that checks the similarity between
        two strings. Returns True when similarity ration is
        bigger than 60% else False

        :param a: The first string for comparison
        :type a: string
        :param b: The second string for comparison
        :type b: string
        :return: The indicated similarity
        :rtype: boolean
        """
        if 1.0 > SequenceMatcher(None, a, b).ratio() >= 0.6:
            return True
        else:
            return False
