# NGram-Tool

### What does it do?
* Finds bigrams, trigrams and fourgrams from from file.
* Calculates frequency distribution of each ngram.
* Discards any similar duplicate with similarity >= 60%
using [SequenceMatcher](https://docs.python.org/2/library/difflib.html#difflib.SequenceMatcher).


### How to install?
* Use python **2.7** only!
* Install Dependencies
    * nltk
* Using `pip2.7 install -r requirements.txt`


### How to use?
* Install the project's requirements
* `python2.7 ngram_tool.py
-i "input file"
* e.g. `python2.7 ngram_tool.py
-f test.txt`
