# Author: Gabriel Chaviaras <chaviaras10@gmail.com>

"""
This file is the main file of ngram_tool project. Is responsible for
argument parsing and some checking. After successfully checking it
calls the NGram_Clac class for calculating the grams taken from
the input file.
"""

from getopt import getopt, GetoptError
from sys import argv
from os import path

from ngram_calc import NGram_Calc


def print_usage():
    """
    A simple function that print a usage message.
    """
    print argv[0] +' -i "input file"'

if __name__ == '__main__':
    try:
        try:
            opts, args = getopt(argv[1:], 'i:', ['input_file='])
        except GetoptError:
            print_usage()
            exit(2)

        input_file = ''

        # extract the arguments from the command line
        for opt, arg in opts:
            if opt in ('-i', '--input_file'):
                input_file = arg


        # check if the input_file is valid
        if not input_file or not path.isfile(input_file):
            print 'Invalid File!'
            print_usage()
            exit(2)

        NGram_Calc(input_file).find_grams()

        exit(True)

    except KeyboardInterrupt:
        exit(False)
